import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Article, ArticleInCart } from '../interfaces/article';

@Injectable({ providedIn: 'root' })
export class CartService {
  articlesInCart: ArticleInCart[] = [];

  /** GET articles from the server */
  getArticlesInCart(): ArticleInCart[]{
    return this.articlesInCart;
  }

  add(articleToBuy: Article): void{
    // Comprobamos que el artículo no exista ya en el carrito para agregarlo
    let coincidences = this.articlesInCart.some(element => articleToBuy.id === element.id);

    if(!coincidences){
      let articleTemp: ArticleInCart;
      articleTemp = {
        id: articleToBuy.id,
        name: articleToBuy.name,
        categoryId: articleToBuy.categoryId,
        description: articleToBuy.description, 
        mainImage: articleToBuy.mainImage,
        price: articleToBuy.price,
        timestamp: 456,
        quantity: 1
      }
      this.articlesInCart.push(articleTemp);
    }
    else{
      this.articlesInCart.filter(function(element){
        if(articleToBuy.id == element.id){
          element.quantity++;
        } 
      });
    }

  }

  remove(id: number): ArticleInCart[]{
    // bajamos la cantidad
    let tempArray = this.articlesInCart.forEach(function(element){
      if(element.id === id){
        element.quantity--;
      }
    });

    this.articlesInCart = this.articlesInCart.filter(element => element.quantity > 0);
    return this.articlesInCart;
  }

  modify(id: string, quantity: number): void{
    
  }

  clear() {
    this.articlesInCart = [];
  }
}
