import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Article } from '../../interfaces/article';
import { ArticleService } from '../../services/article.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  articles: Article[] = [];

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private location: Location
  ) { }

  ngOnInit(): void {
    
    this.getArticlesByCategory();
  }

  getArticlesByCategory(): void {
    let latestId: number;
    this.route.params.subscribe(params => {
      latestId = +params['id'];
      this.articleService.getArticlesByCategory()
      .subscribe(articles => 
        this.articles = articles.filter(article => article.categoryId === latestId).sort((a, b) => b.timestamp - a.timestamp)
      );
    });
  }  

}
