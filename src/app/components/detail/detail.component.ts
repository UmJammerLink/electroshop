import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Article } from '../../interfaces/article';
import { ArticleService } from '../../services/article.service';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: [ './detail.component.css' ]
})
export class DetailComponent implements OnInit {
  article: Article;

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private cartService: CartService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getArticle();
  }

  getArticle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    const characterType = this.route.snapshot.params.characterType;
    this.articleService.getArticle(id).subscribe(article => this.article = article);
  }

  addToCart():void {
    this.cartService.add(this.article);
  }

}
