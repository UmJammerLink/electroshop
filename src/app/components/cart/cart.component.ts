import { Component, OnInit } from '@angular/core';

import { CartService } from '../../services/cart.service';
import { ArticleInCart } from '../../interfaces/article';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  articlesInCart: ArticleInCart[] = [];
  totalPrice: number;

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.getCart();
  }

  getCart(): void{
    this.articlesInCart = this.cartService.getArticlesInCart();
    this.totalPrice = this.articlesInCart.reduce((sum, element) => sum + (element.price * element.quantity), 0);
  }

  deleteItem(id): void{
    this.articlesInCart = this.cartService.remove(id);
    this.totalPrice = this.articlesInCart.reduce((sum, element) => sum + (element.price * element.quantity), 0);
  }

}
