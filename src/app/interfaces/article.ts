export interface Article {
  id: number;
  name: string;
  categoryId: number;
  mainImage: string;
  description: string;
  price: number;
  timestamp: number;
}

export interface ArticleInCart extends Article{
  quantity: number;
}